// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;

/**
 * A class representing a version of a document, storing the original file and/or the delta file.
 */
public class Version {
  private File versionFile;
  private File deltaFile;

  /**
   * Instantiates a new Version with the given versionFile and deltaFile parameters, normally you'll only want to set one of these
   * and use null for the other.
   *
   * @param versionFile this is a File representation of the version of the document
   * @param deltaFile this is a File represntation of the delta between this version and the previous
   */
  public Version(File versionFile, File deltaFile) {
    this.versionFile= versionFile;
    this.deltaFile= deltaFile;
  }

  /**
   * This returns the File representation of the Version, or null if none is set.
   *
   * @return the File representation of the Version, or null if none is set
   */
  public File getVersionFile() {
    return versionFile;
  }

  /**
   * This returns the File representation of the delta between this Version and the previous Version.
   *
   * @return the File representation of the delta between this Version and the previous Version
   */
  public File getDeltaFile() {
    return deltaFile;
  }
}
