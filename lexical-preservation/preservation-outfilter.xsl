<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (c) 2011-2013 DeltaXML Ltd.  All rights reserved. -->

<!--
  This filter is intended to serialize the final document, recreating those XML constrcts that were preserved by the
  LexicalPreservation filter (e.g. doctype, internal subset, comments, processing instructions, and entity references) 
  that have not already been processed into another form by the preceding filters. A consequence of the serialization
  aspect of its intended use is that this filter must be the last filter of the pipeline. 
  
  If it is important to post process the results of this filter, it is best to reparse the output, that way the
  character-maps, doctype, etc. that are only procuced during serialisation will have been applied to the output.
  
  This filter assumes that the processing of the differences, as identified by the deltaxml:deltaV2 attributes, has
  been completed. In particular, this filter does not use the deltaxml:deltaV2 attributes to control its processing.
  Therefore, if both an 'A' and 'B' versions of a node are in the document, they will typically both be output. Doing,
  this may not be consistent with the DTD/Schema rules that the output document complies with. In general, if this
  assumption is broken this filter may produce surprising results or throw an exception when run.  
  
  Additional support for processing the lexical preservation items is provided by the entity-change-resolver.xsl,
  preservation-item-ignore-changes-marker.xsl, and remove-defaulted-attributes.xsl filters, which: resolves how differences
  in entity references are handled; marks whether the 'A' or 'B' values of the preservation item should be used; and
  removes attributes added by the parsers processing the DTD/Schema respectively. These filters are used within our own
  document pipeline's for simplifying the processing of the lexical preservation items. For example, some document formats
  do not enable differences in comments or processing instructions to be displayed. In this situation we, provide the
  facility of saying whether the 'A' or 'B' version of the comment or processing instruction should be used when they
  are modified, and whether the 'A' and/or 'B' version should be used when they are added or deleted. 
 
  Notes:
  (1) This filter strips deltaxml:* and dxwarn2:* elements and attributes if, and only if, the @deltaxml:version attribute
      does not exist on the root element of the document.
  (2) This filter makes use of Michael Kay's XSLT2 variant of output escaping scheme (the last two pages of Chapter 15 of 
      the 4ed of his XSLT book).
-->

<!-- 
  The entities introduced for representing the 5 in-built entities according to Kay's XSLT2 output escaping scheme.
-->
<!DOCTYPE xsl:stylesheet [
  <!ENTITY doe-lt "&#xE801;">
  <!ENTITY doe-amp "&#xE802;">
  <!ENTITY doe-gt "&#xE803;">
  <!ENTITY doe-apos "&#xE804;">
  <!ENTITY doe-quot "&#xE805;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  xmlns:preserve="http://www.deltaxml.com/ns/preserve" 
  xmlns:local="http://www.deltaxml.com/ns/local"
  xmlns:er="http://www.deltaxml.com/ns/entity-references"
  xmlns:pi="http://www.deltaxml.com/ns/processing-instructions"
  xmlns:dxwarn2="http://www.deltaxml.com/ns/warning-for-2-way-merge"
  xmlns:saxon="http://saxon.sf.net/" xmlns:dtd="http://saxon.sf.net/dtd"
  xmlns:dxa="http://www.deltaxml.com/ns/non-namespaced-attribute"
  exclude-result-prefixes="#all" version="2.0">

  <xsl:import href="functions/mode-function.xsl"/>
  
  <xsl:output method="xml"/>

  <xsl:param as="xs:string" name="default-mode" select="'normal'" />
  <xsl:param as="xs:string" name="xmldecl-mode" select="'auto'" />
  <xsl:param as="xs:string" name="doctype-mode" select="'auto'" />
  <xsl:param as="xs:string" name="comment-mode" select="'auto'" />
  <xsl:param as="xs:string" name="pi-mode" select="'auto'" />
  <xsl:param as="xs:string" name="piAndComment-mode" select="'auto'" />
  <xsl:param as="xs:string" name="default-attributes-mode" select="'auto'" />
  <xsl:param as="xs:string" name="cdata-mode" select="'auto'" />
  <xsl:param as="xs:string" name="entity-ref-mode" select="'auto'" />
  <xsl:param as="xs:string" name="ignorable-mode" select="'auto'" />
    
  <!-- this parameter is for internal use only. It should not be removed or modified -->
  <xsl:param name="run-encoding-filter"/>
  
  <xsl:param name="doctype-public-override" as="xs:string" select="''"/>
  <xsl:param name="doctype-system-override" as="xs:string" select="''"/>
  
  <xsl:variable as="xs:string" name="defaultMode" select="if ($default-mode='auto') then 'normal' else $default-mode" />
  <xsl:variable as="xs:boolean" name="processXmlDecl" select="preserve:style($xmldecl-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processDoctype" select="preserve:style($doctype-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processComment" select="preserve:style($comment-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processPi" select="preserve:style($pi-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processPiAndComment" select="preserve:style($piAndComment-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processDefAtts" select="preserve:style($default-attributes-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processCdata" select="preserve:style($cdata-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processEntityRef" select="preserve:style($entity-ref-mode) != 'encoded'"/>
  <xsl:variable as="xs:boolean" name="processIgnorable" select="preserve:style($ignorable-mode) != 'encoded'"/>
  
  
  <xsl:variable name="strip-deltaxml" select="not(exists(/*/@deltaxml:version))" as="xs:boolean" /> 
  
  <!-- 
    The character map used as part of Kay's XSLT2 output escaping scheme
    
    NOTE: Java code overrides the 'disable-escaping' character map to allow
    other character maps to be added from the last
    user-defined XSLT filter. IF the declaration here is modified in any way,
    changes should also be made to:
    com.deltaxml.cores9api.saxons.SaxonCommon.addCharacterMapsToSerializer
  -->
  <xsl:output use-character-maps="disable-escaping" /> 
  <xsl:character-map name="disable-escaping">
    <xsl:output-character character="&doe-lt;" string="&lt;"/>
    <xsl:output-character character="&doe-amp;" string="&amp;"/>
    <xsl:output-character character="&doe-gt;" string="&gt;"/>
    <xsl:output-character character="&doe-apos;" string="&apos;"/>
    <xsl:output-character character="&doe-quot;" string="&quot;"/>
  </xsl:character-map>


  <!-- GENERAL OUTPUT DOCUMENT LAYOUT -->
  
  <xsl:template match="/*">
    <xsl:apply-templates select="preserve:pi-and-comment[@region='BEFORE_DTD']" mode="outside-root"/>
    <xsl:call-template name="output-doctype"/>
    <xsl:apply-templates select="preserve:pi-and-comment[@region='AFTER_DTD']" mode="outside-root"/>
    
    <xsl:copy copy-namespaces="no"> 
      <xsl:choose>
        <xsl:when test="$strip-deltaxml">
          <xsl:copy-of select="namespace::*[not(starts-with(string(), 'http://www.deltaxml.com/ns/'))]"/>          
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="namespace::*"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
    
    <xsl:apply-templates select="preserve:pi-and-comment[@region='AFTER_BODY']" mode="outside-root"/>    
  </xsl:template>
  
  
  <!-- PRE/POST COMMENT PRESERVATION -->   
  
  <xsl:template match="preserve:pi-and-comment" mode="outside-root">
    <xsl:if test="$processPiAndComment=true()">
      <xsl:apply-templates />        
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="preserve:pi-and-comment[$processPiAndComment=true()]" />
 
  
  <!-- GENERAL BODY FILTERS -->
  
  <!-- 
    Copy atribute and text nodes.
  -->
  <xsl:template match="@*|text()">
    <xsl:copy>
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>

  <!-- 
    Copy elements, but remove DeltaXML namespace declarations
  
    The identity transform (using xsl:copy, as per section 7 of the xslt 1.0 spec) copies all namespace nodes
    from the source element to the result element.  Rather than use xsl:copy we need to explicitly create the
    element and do a controlled copy of the namespace nodes.  See the namespaces section of the XSLT FAQ:
    http://www.dpawson.co.uk/xsl/sect2/N5536.html and in particular Q37 and A40, and XSLT 2nd Ed. (Michael Kay)
    page 82.
    
    There doesn't seem to be a way of refering to the namespace nodes in the input tree using the xmlns: prefix
    definitions at the top of this file, instead we can access either the prefixes or the namespace URL used in
    the input tree
  -->
  <xsl:template match="*">
    <xsl:element name="{name()}" namespace="{namespace-uri()}">
      <xsl:copy-of select="namespace::*[not(starts-with(string(), 'http://www.deltaxml.com/ns/'))]"/>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
  </xsl:template>

  <!-- 
    Copy comments and processing instructions that are not inside an entity expansion.
    Add a newline character after each comment or processing instruction in the pre/post-section.
    Note newline characters are only added if both the indent-output and indent-extension variables are true.
  -->
  <xsl:template match="comment()|processing-instruction()">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
    <xsl:if test="parent::preserve:pi-and-comment">
      <xsl:text>&#xA;</xsl:text>
    </xsl:if>  
  </xsl:template>

  <!--
    Recreate preserved Process Instructions
  -->
  <xsl:template match="pi:*[$processPi=true() or (parent::preserve:pi-and-comment and $processPiAndComment=true())]">
    <xsl:processing-instruction name="{local-name()}" select="string(.)"/>
    <xsl:if test="parent::preserve:pi-and-comment">
      <xsl:text>&#xA;</xsl:text>
    </xsl:if>  
  </xsl:template>
    
  <!--
    Recreate preserved Comment data.
  -->
  <xsl:template match="preserve:comment[$processComment=true() or (parent::preserve:pi-and-comment and $processPiAndComment=true())]">
    <xsl:text>&doe-lt;!--</xsl:text>
    <xsl:value-of select="local:disable-escaping(string-join(node(), ''))" />
    <xsl:text>--&doe-gt;</xsl:text>
    <xsl:if test="parent::preserve:pi-and-comment">
      <xsl:text>&#xA;</xsl:text>
    </xsl:if>  
  </xsl:template>
    
  <!--
    Recreate preserved CDATA element.
  -->
  <xsl:template match="preserve:cdata[$processCdata=true()]">
    <xsl:text>&doe-lt;![CDATA[</xsl:text>
    <xsl:value-of select="local:disable-escaping(string-join(node(), ''))" />
    <xsl:text>]]&doe-gt;</xsl:text>
  </xsl:template>
  
  <!--
    Recreate ignorable whitespace - by removing wrapper element
  -->
  <xsl:template match="preserve:ignorable[$processIgnorable]">
    <xsl:value-of select="."/>
  </xsl:template>
  
  <!-- 
    Recreate preserved entity references
  -->
  <xsl:template match="er:*[$processEntityRef=true()]">
    <xsl:value-of select="local:entity-ref(local-name())"/>
  </xsl:template>
  
  <xsl:template match="preserve:entity-group[$processEntityRef=true()]" mode="#all">
    <xsl:apply-templates select="node()[1]" mode="#current" />
  </xsl:template>

  <!-- rRmove xmldecl as the information will already have been passed to the Serializer -->
  <xsl:template match="preserve:xmldecl[$processXmlDecl=true()]"/>

  <!-- 
    Strip DeltaXML attributes except preserve:grammar and preserve:mixed 
  -->
  <xsl:template match="@preserve:*[$processDefAtts=true()]">
    <xsl:if test="local-name() = ('grammar','mixed-content')">
      <xsl:copy/>
    </xsl:if>
  </xsl:template>
  <xsl:template match="@deltaxml:* | @dxwarn2:*">
    <xsl:if test="not($strip-deltaxml)">
      <xsl:copy />
    </xsl:if>            
  </xsl:template>

  <!-- Strip DeltaXML elements -->
  <!-- priority reduced because the predicate seems to elevate it above plain element name matches -->
    <xsl:template match="deltaxml:* | dxwarn2:*">
    <xsl:if test="not($strip-deltaxml)">
      <xsl:element name="{name()}" namespace="{namespace-uri()}">
        <xsl:copy-of select="namespace::*[not(starts-with(string(), 'http://www.deltaxml.com/ns/'))]"/>
        <xsl:apply-templates select="@*|node()"/>
      </xsl:element> 
    </xsl:if>      
  </xsl:template>

  <!-- DOCTYPE PROCESSING -->
  <!--
    The templates in this section extract the infromation stored in the preserve:doctype and output this information
    as the doctype and inernal subset.
    
    Note that by this point the version information should have been removed from the preserve:doctype element and
    its children. Support for this is provided by the preservation-item-ignore-changes-marker.xsl filter, which marks
    whether the 'A' or 'B' values of the doctype and internal subset settings should be used. These marked setting are
    then processed through the apply-ignore-changes.xsl and propogate-ignore-changes.xsl filters as discussed in the
    'How to ignore changes' sample.

    Currently we use Saxon's doctype extension, but this does not provide a means to output comments, processing
    instructions and entity references in the internal subset. Entity references are probably going to be the most
    significant of these omissions.
    
    It would be possible to use an approach where we explicitly output the characters that appear in the serialised
    output stream.
  -->
  
  <!--
    Recreate the preserved doctype.
  -->
  <xsl:template name="output-doctype">
    <xsl:if test="string-length($doctype-system-override) gt 0 or
                  string-length($doctype-public-override) gt 0 or
                  (exists(preserve:doctype) and $processDoctype=true())">
      <saxon:doctype xsl:extension-element-prefixes="saxon"
                   xsl:use-when="element-available('saxon:doctype')">
      <dtd:doctype name="{if (string-length(preserve:doctype/@name) gt 0) then preserve:doctype/@name else name(/*)}">
        <xsl:choose>
          <xsl:when test="string-length($doctype-system-override) gt 0">
            <xsl:attribute name="system" select="$doctype-system-override"/>
          </xsl:when>
          <xsl:when test="preserve:doctype/@systemId">
            <xsl:attribute name="system" select="preserve:doctype/@systemId"/>
          </xsl:when>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="string-length($doctype-public-override) gt 0">
            <xsl:attribute name="public" select="$doctype-public-override"/>
          </xsl:when>
          <xsl:when test="preserve:doctype/@publicId">
            <xsl:attribute name="public" select="preserve:doctype/@publicId"/>
          </xsl:when>
        </xsl:choose>
        <!-- 
          Cannot handle text, comments, or processing instructions within a preserved doctype, thus we
          choose the elements.
        -->
        <xsl:if test="$processDoctype">
          <xsl:for-each-group select="preserve:doctype/*" group-adjacent="node-name(.)">
            <xsl:apply-templates select="." mode="doctype"/>
          </xsl:for-each-group>
        </xsl:if>
      </dtd:doctype>
    </saxon:doctype>
    <xsl:message use-when="not(element-available('saxon:doctype'))"
      select="'Warning: Ignoring DOCTYPE; preservation of a DOCTYPE requires a licensed version of either Saxon PE or EE.'"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="preserve:doctype[$processDoctype=true()]"/>
   
  <!--
    Recreate preserved attribute declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:attributeDecl" mode="doctype">
    <xsl:for-each-group select="current-group()" group-adjacent="@eName">
      <dtd:attlist element="{@eName}">
        <xsl:apply-templates select="current-group()" mode="doctype-inner"/>
      </dtd:attlist>
    </xsl:for-each-group>
  </xsl:template>

  <xsl:template match="preserve:attributeDecl" mode="doctype-inner">
    <xsl:variable name="val" select="if (exists(@value)) then local:quote-string(@value) else ''"/>
    <xsl:variable name="sep" select="if (exists(@mode) and exists(@value)) then ' ' else ''"/>
    <dtd:attribute name="{@name}" type="{@type}" value="{concat(@mode,$sep,$val)}"/>
  </xsl:template>

  <!--
    Recreate preserved non attribute nodes (in the doctype's internal subset)
  -->
  <xsl:template match="node()" mode="doctype">      
    <xsl:apply-templates select="current-group()" mode="doctype-inner"/>
  </xsl:template>

  <!--
    Recreate preserved element delcarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:elementDecl" mode="doctype-inner">
    <dtd:element name="{@name}" content="{@model}"/>
  </xsl:template>

  <!-- 
    Recreate preserved internal parsed parameter entity declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:internalParsedParameterEntityDecl" mode="doctype-inner">
    <dtd:entity name="{@name}" parameter="yes">
      <xsl:value-of select="local:quote-string(local:decode-entities-in-string(@value))"/>
    </dtd:entity>
  </xsl:template>

  <!-- 
    Recreate preserved internal parsed general entity declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:internalParsedGeneralEntityDecl" mode="doctype-inner">
    <xsl:variable name="entity-name" select="@name"/>
    <xsl:choose>
      <xsl:when test="@value">
        <dtd:entity name="{$entity-name}" parameter="no">
          <xsl:value-of select="local:quote-string(local:decode-entities-in-string(@value))"/>
        </dtd:entity>
      </xsl:when> 
      <xsl:otherwise>
        <xsl:for-each select="deltaxml:attributes/dxa:value/deltaxml:attributeValue">
          <dtd:entity name="{$entity-name}" parameter="no">
            <xsl:value-of select="local:quote-string(local:decode-entities-in-string(current()))"/>
          </dtd:entity>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- 
    Recreate preserved external parsed parameter entity declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:externalParsedParameterEntityDecl" mode="doctype-inner">
    <dtd:entity name="{@name}" parameter="yes">
      <xsl:call-template name="external"/>
    </dtd:entity>
  </xsl:template>

  <!-- 
    Recreate preserved external parsed general entity declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:externalParsedGeneralEntityDecl" mode="doctype-inner">
    <dtd:entity name="{@name}" parameter="no">
      <xsl:call-template name="external"/>
    </dtd:entity>
  </xsl:template>

  <!-- 
    Recreate preserved unparsed parameter entity declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:unparsedEntityDecl" mode="doctype-inner">
    <dtd:entity name="{@name}" parameter="no" notation="{@notationName}">
      <xsl:call-template name="external"/>
    </dtd:entity>
  </xsl:template>

  <!-- 
    Recreate preserved notation declarations (in the doctype's internal subset)
  -->
  <xsl:template match="preserve:notationDecl" mode="doctype-inner">
    <dtd:notation name="{@name}">
      <xsl:call-template name="external"/>
    </dtd:notation>
  </xsl:template>
  
  <xsl:template name="external">
    <xsl:if test="@systemId">
      <xsl:attribute name="system" select="@systemId"/>
    </xsl:if>
    <xsl:if test="@publicId">
      <xsl:attribute name="public" select="@publicId"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="preserve:comment" mode="doctype-inner">
    <xsl:value-of select="concat('&#xA;  &lt;!--', text(), '--&gt;')" />
  </xsl:template>

  <xsl:template match="pi:*" mode="doctype-inner">
    <xsl:value-of select="concat('&#xA;  &lt;?', local-name(), ' ', text(), '?&gt;')"/>
  </xsl:template>
  
  <xsl:template match="er:*" mode="doctype-inner">
    <xsl:value-of select="concat('&#xA;  ', (if (@parameter='yes') then '%' else '&amp;'), local-name(), ';')"/>
  </xsl:template>
    
  
  <!-- HELPER FUNCTIONS -->

  <!--
    This funcion outputs an entity reference with the given name.
  -->
  <xsl:function name="local:entity-ref" as="xs:string">
    <xsl:param name="text" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="$text='*lt'">
        <xsl:value-of select="string('&doe-lt;')"/>        
      </xsl:when>
      <xsl:when test="$text='*gt'">
        <xsl:value-of select="string('&doe-gt;')"/>        
      </xsl:when>
      <xsl:when test="$text='*apos'">
        <xsl:value-of select="string('&doe-apos;')"/>        
      </xsl:when>
      <xsl:when test="$text='*quot'">
        <xsl:value-of select="string('&doe-quot;')"/>        
      </xsl:when>
      <xsl:when test="$text='*amp'">
        <xsl:value-of select="string('&doe-amp;')"/>        
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat(string('&doe-amp;'), $text, ';')"/>        
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--
    This function decodes entity usage within attribute strings used to represent internal entity declarations that
    are originally encoded by the EntityToXML filter
  -->
  <xsl:function name="local:decode-entities-in-string" as="xs:string">
    <xsl:param name="text" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="contains($text, '!')">
        <xsl:variable name="components" select="tokenize(replace($text, '!!', '!'), '![()]')"/>
        <xsl:value-of
          select="string-join(for $i in (1 to count($components)) return if (($i mod 2)=0) then local:entity-ref($components[$i]) else $components[$i], '')"
        />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!--
    A function that attempts to appropriatelty quote a string that may itself contain quote characters.
    It only works if the string contains either single or double quote characters, but not both.
    
    Note, this ought to be the case if this filter is used in conjuction with the EntityToXML filter, which
    translates the contents of attributes into a string.
  -->
  <xsl:function name="local:quote-string" as="xs:string">
    <xsl:param name="text" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="contains($text, '&quot;') or contains($text, '&doe-quot;')">
        <xsl:value-of select="concat(&quot;&apos;&quot;, $text, &quot;&apos;&quot;)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat('&quot;', $text, '&quot;')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--
    A varaint of the function used to disable-output-escaping on a string, according to Kay's XSLT2 output escaping scheme.
  -->
  <xsl:function name="local:disable-escaping" as="xs:string">
    <xsl:param name="in" as="xs:string" />
    <xsl:sequence select="translate( $in, '&lt;&amp;&gt;''&quot;', '&doe-lt;&doe-amp;&doe-gt;&doe-apos;&doe-quot;' )" />
  </xsl:function>

</xsl:stylesheet>
