<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:preserve="http://www.deltaxml.com/ns/preserve"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                exclude-result-prefixes="xs"
                version="2.0">
  
  <!--<xsl:variable name="defaultMode" select="'BdA'"/>-->
  
  <xsl:function name="preserve:style" as="xs:string">
    <xsl:param name="inMode" as="xs:string" />
    
    <xsl:choose>
      <xsl:when test="$inMode='auto'">
        <xsl:value-of select="$defaultMode"/>
      </xsl:when>
      <xsl:when test="$inMode='normal'">
        <xsl:value-of select="'normal'"/>
      </xsl:when>
      <xsl:when test="$inMode='encoded'">
        <xsl:value-of select="'encoded'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message
          select="concat('Invalid mode ', $inMode,
          ' was expecting one of ', '''normal'', ''encoded'', ''auto''.')"
          terminate="yes"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
   <xsl:function name="preserve:mode" as="xs:string">
    <xsl:param name="inMode" as="xs:string"/>
    <xsl:param name="isDecl" as="xs:boolean" />
     <xsl:value-of select="preserve:mode($inMode, $isDecl, '')"/>
   </xsl:function>
  
  <xsl:function name="preserve:mode" as="xs:string">
    <xsl:param name="inMode" as="xs:string" />
    <xsl:param name="isDecl" as="xs:boolean" />
    <xsl:param name="extra-values" as="xs:string" />
    
    <xsl:choose>
      <xsl:when test="$inMode='auto'">
        <xsl:value-of select="preserve:mode($defaultMode, $isDecl)"/>
      </xsl:when>
      <xsl:when test="$inMode='skip'">
        <xsl:value-of select="'skip'"/>
      </xsl:when>
      <xsl:when test="$inMode='A' or (not($isDecl) and $inMode='AdB')">
        <xsl:value-of select="'A'"/>
      </xsl:when>
      <xsl:when test="$inMode='B' or (not($isDecl) and $inMode='BdA')">
        <xsl:value-of select="'B'"/>
      </xsl:when>
      <xsl:when test="$inMode=('A,B', 'AB') or ($isDecl and $inMode='AdB')">
        <xsl:value-of select="'A,B'"/>
      </xsl:when>
      <xsl:when test="$inMode=('B,A', 'BA') or ($isDecl and $inMode='BdA')">
        <xsl:value-of select="'B,A'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message
          select="concat('Invalid mode ', $inMode,
          ' was expecting one of ', $extra-values, '''A'', ''B'', ''A,B'', ''B,A'', ''AB'', ''BA'', ''AdB'', ''BdA'', ''skip'', or ''auto''.')"
          terminate="yes"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="preserve:defaultAttMode" as="xs:string">
    <xsl:param name="inMode" as="xs:string"></xsl:param>
    <xsl:choose>
      <xsl:when test="$inMode=('explicit', 'default')">
        <xsl:value-of select="$inMode"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="preserve:mode($inMode, false(), '''default'', ''explicit''')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  
</xsl:stylesheet>