# Using Deltas For XML Versioning (diff and patch)
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

A simple code sample showing how to use a mixture of comparison and recombination demonstrating how XML Compare can be used manage XML versioning.

This document describes how to run the sample. For concept details see: [Using Deltas for XML Versioning (diff and patch)](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-deltas-for-xml-versioning-diff-and-patch)

The sample loads 5 versions of a document, generates change-only deltas between the versions and reconstructs the requested version (version 3 by default).
The sample is designed to be built and run via the Ant build technology. The provided build.xml script has two main targets

- *run* - the default target which compiles and runs the sample code.
- *clean* - returns the sample to its original state.
  
It can be run by issuing either the `ant` or the `ant run` commands. If you wish to override which version of the document is included then you should run `ant -Dversion-to-retrieve=n` (where n is the version you wish to see retrieved).
Alternatively, the sample can be manually compiled and run using the following Java commands, asuming that both the Java compiler and runtime platforms are avaialble on the command-line.
	
	javac -cp ../../deltaxml-x.y.z.jar:../../saxon9pe.jar DeltasForVersioning.java Version.java
	java -cp .:../../deltaxml-x.y.z.jar:../../saxon9pe.jar DeltasForVersioning 3
	
Note: You should ensure that you use the correct directory and class path separators for your operating system. Also, replace *x.y.z* in the *deltaxml-x.y.z jar* name with the required major.minor.patch numbers, for example: *deltaxml-10.0.0.jar*