// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltTransformer;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.deltaxml.api.XMLCombiner;
import com.deltaxml.api.XMLCombinerFactory;
import com.deltaxml.cores9api.DXPConfigurationS9;
import com.deltaxml.cores9api.PipelinedComparatorS9;
import com.deltaxml.cores9api.config.LexicalPreservationConfig;
import com.deltaxml.pipe.filters.LexicalPreservationBase;

/**
 * A class supplying an simple Delta For Versioning implementation
 */
public class DeltasForVersioning {
  private List<Version> versions= new ArrayList<Version>();

  private PipelinedComparatorS9 rawComparator;
  private PipelinedComparatorS9 deltaValidateComparator;
  private XMLCombiner combiner;

  private File resultFolder= new File("result");
  private File deltaFolder= new File(resultFolder, "deltas");
  private File tmpProcessedInputsFolder= new File("processedInputs");
  private File tmpForwardsFolder= new File(resultFolder, "tmp/forwards");
  private File tmpBackwardsFolder= new File(resultFolder, "tmp/backwards");

  private final boolean useLexicalPreservationFilters;

  /**
   * This initiates a new DeltasForVersioning.
   *
   * @param useLexicalPreservationFilters This configures whether to use the lexical preservation filters, see
   *          http://docs.deltaxml.com/xml-compare/current/docs/api/com/deltaxml/pipe/filters/LexicalPreservation.html}.
   *
   * @throws Exception this is thrown if there is a problem instantiating the comparator or combiner
   */
  public DeltasForVersioning(boolean useLexicalPreservationFilters) throws Exception {
    this.useLexicalPreservationFilters= useLexicalPreservationFilters;

    // configure the comparator used for creating recombinable results
    DXPConfigurationS9 rawDxp= new DXPConfigurationS9(new File("dxp/compare-raw.dxp"));
    rawComparator= rawDxp.generate();

    // configure the comparator used for checking the generated deltas
    DXPConfigurationS9 checkDxp= new DXPConfigurationS9(new File("version-check.dxp"));
    deltaValidateComparator= checkDxp.generate();

    // configure the combiner
    combiner= XMLCombinerFactory.newInstance().newXMLCombiner();

    // This feature prevents the DeltaXML output generation comment being added at the top of the recombine result
    combiner.setFeature("http://deltaxml.com/api/feature/outputHeaderComment", false);
  }

  /**
   * This is a utility method for making the running the combiner simpler.
   *
   * @param originalFile input file to run the combiner on
   * @param deltaFile delta file to run the combiner with
   * @param outputFile file to save the result to
   * @param forwardsDirection set to true for forward combine, or false for reverse
   *
   * @throws Exception thrown if the combiner fails
   * @see XMLCombiner#combine(javax.xml.transform.Source, javax.xml.transform.Source, javax.xml.transform.Result)
   */
  private void combine(File originalFile, File deltaFile, File outputFile, boolean forwardsDirection) throws Exception {
    combiner.setFeature("http://deltaxml.com/api/feature/isCombineForward", forwardsDirection);
    combiner.combine(new StreamSource(originalFile),
                     new StreamSource(deltaFile),
                     new StreamResult(new FileOutputStream(outputFile)));
  }

  /**
   * This method adds the supplied File as a new version of the document.
   *
   * @param document file to add as a version
   *
   * @throws Exception thrown if there is a problem adding the version
   */
  public void addVersion(File document) throws Exception {
    Version newVersion;

    // Run any input processing on the document to add
    document= runLexicalPreservationInfilter(document);

    if (versions.isEmpty()) {
      // first version is stored without a delta
      System.out.println(" * Adding version 1");
      newVersion= new Version(document, null);
    } else {
      // for subsequent versions, a delta is created between the new version and the previous
      System.out.println(" * Adding version " + (versions.size() + 1) + " and creating a changes-only delta with version " + versions.size());

      // Create the delta folder and sub-folders, if necessary
      deltaFolder.mkdirs();

      File deltaFile= new File(deltaFolder, "delta-v" + (versions.size()) + "-to-v" + (versions.size() + 1) + ".xml");
      File latestVersionFile= versions.get(versions.size() - 1).getVersionFile();

      deltaValidateComparator.compare(latestVersionFile, document, deltaFile);

      try {
        verifyDeltaForInput(latestVersionFile, document, deltaFile);
      } catch (Exception e) {
        throw new Exception("Unable to add document due to verification failure", e);
      }

      newVersion= new Version(document, deltaFile);

      // in a real system, we would remove the stored full copy of the document for the previous version at this point
    }
    versions.add(newVersion);
  }

  /**
   * When using lexical preservation, this method runs the lexical preservation infilter on each version prior to doing the raw
   * comparison. The recombine must be run on exactly the same input that the delta was generated for, so for this reason we
   * suggest storing the complete version of the document with this filter applied.
   *
   * Note: The outfilter for DocType preservation won't work without a SaxonPE license.
   *
   * @param input The input to run the lexical preservation infilter on
   *
   * @return The input with the lexical preservation infilter run
   */
  private File runLexicalPreservationInfilter(File input) throws IOException, SAXException, TransformerException {
    // we don't process the input if we're not using lexical preservation
    if (!useLexicalPreservationFilters) {
      return input;
    }

    System.out.println(" * running LexicalPreservation infilter on " + input.getName());

    // store the file in the tmpProcessedInputsFolder
    tmpProcessedInputsFolder.mkdirs();
    File filteredFile= new File(tmpProcessedInputsFolder, input.getName());

    // Create parser
    XMLReader parser= XMLReaderFactory.createXMLReader();

    // Create InputSource
    InputSource is= new InputSource(new FileInputStream(input));

    // configure lexical preservation infilter    
    LexicalPreservationConfig lexicalPreservationConfig= new LexicalPreservationConfig();
    lexicalPreservationConfig.setAllPreservationItems(true);
    
    LexicalPreservationBase lexicalPreservationFilter= new ConfiguredLexicalPreservation(lexicalPreservationConfig);
    lexicalPreservationFilter.setParent(parser);

    // run filters
    SAXSource source= new SAXSource(lexicalPreservationFilter, is);

    Transformer transformer= TransformerFactory.newInstance().newTransformer();
    transformer.transform(source, new StreamResult(filteredFile));

    return filteredFile;
  }

  /**
   * Check that the supplied supplied delta is valid for the supplied 'A' and 'B' inputs. The validation checks that the recombine
   * is successful, and that the recombines produce the same inputs. While this is potentially a costly exercise, it is worthwhile
   * if you're throwing the original inputs away and need confidence to be able to regenerate them precisely.
   *
   * @param inputA The original 'A' input
   * @param inputB The original 'B' input
   * @param changesOnlyDelta The changes-only deltaV2 for the supplied 'A' and 'B inputs.
   *
   * @throws IOException This is thrown when creating the temporary files to hold the recombine output
   * @throws InvalidChangesOnlyDeltaException This is thrown if the changesOnlyDelta failed to be verified
   */
  private void verifyDeltaForInput(File inputA, File inputB, File changesOnlyDelta) throws IOException,
      InvalidChangesOnlyDeltaException {
    verifyDeltaInDirection(inputA, inputB, changesOnlyDelta, true);
    verifyDeltaInDirection(inputB, inputA, changesOnlyDelta, false);
  }

  /**
   *
   * @param input The original input file. This is the 'A' input if forwardsDirection is true, and the 'B' input if it is false.
   * @param originalTarget The original target file. This is the 'B' input if forwardsDirection is true, and the 'A' inout if it
   *          is false.
   * @param changesOnlyDelta The changes-only DeltaV2.
   * @param forwardsDirection Whether to perform a forwards recombine, supplying false will do a reverse recombine.
   *
   * @throws IOException This is thrown when creating the temporary files to hold the recombine output
   * @throws InvalidChangesOnlyDeltaException This is thrown if the changesOnlyDelta failed to be verified
   */
  private void verifyDeltaInDirection(File input, File originalTarget, File changesOnlyDelta, boolean forwardsDirection)
      throws IOException, InvalidChangesOnlyDeltaException {
    String inputLetter= forwardsDirection ? "A" : "B";
    String targetLetter= forwardsDirection ? "B" : "A";

    File regeneratedTarget= File.createTempFile("regenerated-" + targetLetter, ".xml");

    try {
      // Regenerate target by recombining using the supplied input and the changesOnlyDelta
      combine(input, changesOnlyDelta, regeneratedTarget, forwardsDirection);
    } catch (Exception e) {
      throw new InvalidChangesOnlyDeltaException("Failed to do " +
                                                 (forwardsDirection ? "forwards" : "backwards") +
                                                 " -recombine with supplied changes-only Delta", e);
    }

    // Check the regenerated target is the same as the original
    try {
      if (!rawComparator.isEqual(originalTarget, regeneratedTarget)) {
        throw new InvalidChangesOnlyDeltaException("Original '" + inputLetter + "' input is different to regenerated '" +
                                                   targetLetter + "'input");
      }
    } catch (Exception e) {
      throw new InvalidChangesOnlyDeltaException("Could not compare original input '" + inputLetter +
                                                 "' with the regenerated input '" + targetLetter + "'", e);
    }
  }

  /**
   * Retrieves the version of the document using forward combine.
   *
   * @param versionNumber version of the document to retrieve
   * @param resultFile file to save the result of the retrieval
   *
   * @throws Exception thrown if there is a problem retrieving the version of the document
   */
  public void retrieveVersionForwards(int versionNumber, File resultFile) throws Exception {
    tmpForwardsFolder.mkdirs();

    if (versionNumber == 1) {
      // if first version is requested then there is no need to do any recombination
      System.out.println(" * copying version " + versionNumber + " from its original location (no need to recreate)");
      runLexicalPreservationOutfilter(versions.get(0).getVersionFile(), resultFile);
    } else {
      File previousResult= null;

      // iterate for each recombine we need to do
      for (int i= 1; i < versionNumber; i++) {
        System.out.println(" * recreating version " + (i + 1) + " using version " + i);

        // get the previous and next versions
        Version previousVersion= versions.get(i - 1);
        Version currentVersion= versions.get(i);

        // create a file to store the result of the recombination
        File currentResultFile= new File(tmpForwardsFolder, "v" + (i + 1) + "-from-v" + i + ".xml");

        // if this is the first recombination then we want to use the original file, otherwise we use the previous result
        File inputFile;
        if (previousResult == null) {
          inputFile= previousVersion.getVersionFile();
        } else {
          inputFile= previousResult;
        }

        // get the delta required for this recombination
        File deltaFile= currentVersion.getDeltaFile();

        // do the forwards recombination
        combine(inputFile, deltaFile, currentResultFile, true);

        // keep hold of the previous result
        previousResult= currentResultFile;
      }

      // copy last result as the supplied file
      runLexicalPreservationOutfilter(previousResult, resultFile);
    }
  }

  /**
   * Retrieves the version of the document using reverse combine.
   *
   * @param versionNumber version of the document to retrieve
   * @param resultFile file to save the result of the retrieval
   *
   * @throws Exception thrown if there is a problem retrieving the version of the document
   */
  public void retrieveVersionBackwards(int versionNumber, File resultFile) throws Exception {
    tmpBackwardsFolder.mkdirs();

    if (versionNumber == versions.size()) {
      // if last version is requested then there is no need to do any recombination
      System.out.println(" * copying version " + versionNumber + " from its original location (no need to recreate)");
      runLexicalPreservationOutfilter(versions.get(versionNumber - 1).getVersionFile(), resultFile);
    } else {
      File previousResult= null;

      // iterate for each recombine we need to do
      for (int i= versions.size() - 1; i > versionNumber - 1; i--) {
        System.out.println(" * recreating version " + i + " using version " + (i + 1));

        // get the previous and next versions
        Version previousVersion= versions.get(i);

        // create a file to store the result of the recombination
        File currentResultFile= new File(tmpBackwardsFolder, "v" + i + "-from-v" + (i + 1) + ".xml");

        // if this is the first recombination then we want to use the original file, otherwise we use the previous result
        File inputFile;
        if (previousResult == null) {
          inputFile= previousVersion.getVersionFile();
        } else {
          inputFile= previousResult;
        }

        // get the delta required for this recombination
        File deltaFile= previousVersion.getDeltaFile();

        // do the forwards recombination
        combine(inputFile, deltaFile, currentResultFile, false);

        // keep hold of the previous result
        previousResult= currentResultFile;
      }

      // copy last result as the supplied file
      runLexicalPreservationOutfilter(previousResult, resultFile);
    }
  }

  /**
   * This makes a copy of the source file to the destination file.
   *
   * @param sourceFile file to copy from
   * @param destinationFile file to copy to
   *
   * @throws IOException thrown if there is a problem copying the file
   */
  private static void fileCopy(File sourceFile, File destinationFile) throws IOException {
    FileInputStream in= null;
    FileOutputStream out= null;
    try {
      in= new FileInputStream(sourceFile);
      out= new FileOutputStream(destinationFile);

      byte[] b= new byte[1024];
      int read;
      while ((read= in.read(b)) != -1) {
        out.write(b, 0, read);
      }
    } finally {
      try {
        if (in != null) {
          in.close();
        }
      } finally {
        if (out != null) {
          out.close();
        }
      }
    }
  }

  /**
   * Runs the lexical preservation outfilter on the given input and placing the result in the output file. If
   * useLexicalPreservationFilters is false then calls {@link #fileCopy(java.io.File, java.io.File)}.
   *
   * @param input The input to apply lexical-preservation/preservation-outfilter.xsl
   * @param output The output file to store the result.
   */
  private void runLexicalPreservationOutfilter(File input, File output) throws IOException, SaxonApiException {
    if (!useLexicalPreservationFilters) {
      fileCopy(input, output);
    }

    System.out.println(" * running lexical-preservation/preservation-outfilter.xsl");

    // Using s9api as xml-entity-outfilter.xsl requires Saxon to do the serialization
    Processor p= new Processor(false);
    File filter= new File("lexical-preservation/preservation-outfilter.xsl");
    InputSource inputSource= new InputSource(new FileInputStream(filter));
    inputSource.setSystemId(filter.toURI().toURL().toExternalForm());
    XsltTransformer xEO= p.newXsltCompiler().compile(new SAXSource(inputSource)).load();

    XdmNode inputNode= p.newDocumentBuilder().build(input);
    xEO.setInitialContextNode(inputNode);

    xEO.setDestination(p.newSerializer(output));
    xEO.transform();
  }

  /**
   * Main method of the class to run the DeltasForVersioning sample.
   *
   * @param args command line arguments
   *
   * @throws Exception thrown if there is an issue running the sample
   */
  public static void main(String[] args) throws Exception {
    DeltasForVersioning dvc= new DeltasForVersioning(true);

    File inputFolder= new File("inputs");

    // add some sample versions
    System.out.println("Adding sample versions");
    dvc.addVersion(new File(inputFolder, "version1.xml"));
    dvc.addVersion(new File(inputFolder, "version2.xml"));
    dvc.addVersion(new File(inputFolder, "version3.xml"));
    dvc.addVersion(new File(inputFolder, "version4.xml"));
    dvc.addVersion(new File(inputFolder, "version5.xml"));

    if (args.length != 1) {
      outputUsage(dvc);
    } else {
      try {
        int inputNumber= Integer.parseInt(args[0]);
        if (inputNumber < 1 || inputNumber > dvc.versions.size()) {
          System.out.println("\"" + inputNumber + "\" is out of range");
          outputUsage(dvc);
        } else {
          // do a sample forward combine document version retrieval
          System.out.println("Retrieving version " + inputNumber + " forwards");
          File forwardsResultFile= new File(dvc.resultFolder, "result-file-v" + inputNumber + "-forwards.xml");
          dvc.retrieveVersionForwards(inputNumber, forwardsResultFile);
          System.out.println("Result has been saved as " + forwardsResultFile.getPath() + "\n");

          // do a sample reverse combine document version retrieval
          System.out.println("Retrieving version " + inputNumber + " backwards");
          File backwardsResultFile= new File(dvc.resultFolder, "result-file-v" + inputNumber + "-backwards.xml");
          dvc.retrieveVersionBackwards(inputNumber, backwardsResultFile);
          System.out.println("Result has been saved as " + backwardsResultFile.getPath());
        }
      } catch (NumberFormatException e) {
        System.out.println("\"" + args[0] + "\" is not a number!");
        outputUsage(dvc);
      }
    }
  }

  /**
   * Output the usage information for the sample.
   *
   * @param dvc the instance of the DeltasForVersioning sample
   */
  private static void outputUsage(DeltasForVersioning dvc) {
    System.out.println("Usage: java DeltasForVersioning <document-to-retrieve>\n" +
                       "<document-to-retrieve> is a number between 1 and " + dvc.versions.size());
    System.exit(1);
  }
  
  class ConfiguredLexicalPreservation extends LexicalPreservationBase {

    public ConfiguredLexicalPreservation(LexicalPreservationConfig config) {
      super();
      setPreserved(PI_ALL, false);
      setPreserved(config.toPreserveItemEnumSet(), true);
    }

  }
}
