// Copyright (c) 2024 Deltaman group limited. All rights reserved.

/**
 * Exception indicates that the generated changes-only DeltaV2 for two inputs is invalid. This is usually an indication that the
 * Delta could not successfully recreate the original input
 */
public class InvalidChangesOnlyDeltaException extends Exception {
  /**
   * Create a new exception with the given message and cause
   *
   * @param message A message describing the exception
   * @param cause The original cause of the exception
   */
  public InvalidChangesOnlyDeltaException(String message, Exception cause) {
    super(message, cause);
  }

  /**
   * Create a new exception with the given message
   *
   * @param message A message describing the exception
   */
  public InvalidChangesOnlyDeltaException(String message) {
    super(message);
  }
}
